export const DEVELOPMENT_API_URL = 'http://localhost:5000';
export const UAT_API_URL = 'https://service.uat.goexperts.net';
export const PRODUCTION_API_URL = 'https://service.uat.goexperts.net';
export const LAMBDA_GET_IMAGE_URL_BY_KEY = 'https://onqo1b3cga.execute-api.ap-southeast-2.amazonaws.com/test';
