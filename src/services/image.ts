import axios from 'axios';
import { LAMBDA_GET_IMAGE_URL_BY_KEY } from 'constants/apiUrl';

const getImageUrlByKey = async (key: string) => {
  const apiUrl = LAMBDA_GET_IMAGE_URL_BY_KEY;
  try {
    const res = await axios.get(`${apiUrl}?key=${key}`);
    const { tempUrl } = res.data;
    return tempUrl;
  } catch (err) {
    return 'https://picsum.photos/400/300';
  }
};

export default getImageUrlByKey;
