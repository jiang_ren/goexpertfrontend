export interface IBookInfo {
  expertID: string;
  userID: string | null;
  firstName: String | null;
  lastName: String | null;
  email: string | null;
  nameOnCard: String;
  cardNumber: Number | null;
  CVVCode: Number | null;
  expiration: Date | null;
  date: Date;
  time: String| null;
  price: Number;
}
