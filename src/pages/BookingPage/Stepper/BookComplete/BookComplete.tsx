import React from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Button } from '@mui/material';
import { useHistory } from 'react-router-dom';
import styles from './bookComplete.module.scss';

const BookComplete = () => {
  const history = useHistory();

  return (
    <div className={styles.wrapper}>

      <CheckCircleIcon className={styles.checkCircle} />
      <p>
        Your chatting request has been submited to the expert.
      </p>
      <p>
        Please go to Profile - My request to check your request progress.
      </p>
      <Button
        fullWidth
        className={styles.checkRequestButton}
        onClick={() => history.push('/profile')}
      >
        Check Your Request Progress
      </Button>
    </div>
  );
};

export default BookComplete;
