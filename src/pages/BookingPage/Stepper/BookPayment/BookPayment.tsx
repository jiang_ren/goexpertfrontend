import React, { MouseEventHandler, SetStateAction, useState } from 'react';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import {
  Box, Button, Alert,
} from '@mui/material';
import { IBookInfo } from 'types/IMeeting';
import Cleave from 'cleave.js/react';
import styles from './bookPayment.module.scss';

interface BookCustomerInfoProps {
  handlePrevious: MouseEventHandler<HTMLButtonElement>;
  handleSubmit: Function;
  handleChange: Function;
  bookDetail: IBookInfo;
}

const BookPayment: React.FC<BookCustomerInfoProps> = ({
  handlePrevious, handleChange, bookDetail, handleSubmit,
}) => {
  const [creditCardType, setCreditCardType] = useState('');
  const [errMsg, setErrMsg] = useState('');
  const {
    price, nameOnCard, cardNumber, CVVCode, expiration,
  } = bookDetail;

  function onCreditCardTypeChanged(type: SetStateAction<string>) {
    setCreditCardType(type);
  }

  const handleValidation = () => {
    if (!nameOnCard || !cardNumber || !CVVCode || !expiration) {
      return setErrMsg('Please fill a valid card information!');
    }
    return handleSubmit();
  };

  return (
    <div className={styles.wrapper}>
      <h2 className={styles.heading}>Payment</h2>
      <p className={styles.paymentMethod}>Payment Methode</p>
      <div className={styles.icon}>
        <CreditCardIcon />
        <p>Debit / Credit Card Only</p>
      </div>
      <div>
        <input
          required
          className={styles.input}
          placeholder="Name On Card"
          name="nameOnCard"
          onChange={handleChange('nameOnCard')}
        />
        <div className={styles.cardNumber}>
          <Cleave
            placeholder="Enter credit card number"
            options={{
              creditCard: true,
              onCreditCardTypeChanged,
            }}
            onChange={handleChange('cardNumber')}
            className={styles.input}
          />
          {creditCardType.toLowerCase() !== 'unknown' && (
            <p className={styles.cardType}>{creditCardType}</p>
          )}
        </div>
        <Box className={styles.smallInputWrapper}>
          <Cleave
            placeholder="MM/YY"
            options={{ date: true, datePattern: ['m', 'd'] }}
            onChange={handleChange('expiration')}
            className={styles.smallInput}
          />
          <Cleave
            placeholder="CVV"
            options={{
              blocks: [3],
              numericOnly: true,
            }}
            onChange={handleChange('CVVCode')}
            className={styles.smallInput}
          />
        </Box>
        {errMsg ? <Alert severity="error">{errMsg}</Alert> : <></>}
      </div>
      <Box className={styles.buttonWrapper}>
        <Button
          className={styles.previousButton}
          onClick={handlePrevious}
        >
          Previous
        </Button>
        <Button
          className={styles.submitButton}
          onClick={handleValidation}
        >
          {`Confirm Payment $${price}`}
        </Button>
      </Box>
    </div>
  );
};

export default BookPayment;
