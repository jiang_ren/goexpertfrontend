import React, { useState, useEffect } from 'react';
import {
  Box, Stepper, Step, StepLabel, useMediaQuery,
} from '@mui/material';
import ExpertCard from 'components/ExpertCard';
import { fetchExpertById, selectCurrentExpertDetails } from 'store/slices/expertsSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import {
  selectCurrentUserEmail,
  selectCurrentUserLastName,
  selectCurrentUserFirstName,
  selectCurrentUserId,
} from 'store/slices/userSlice';
import { createMeeting } from 'store/slices/meetingSlice';
import BookCustomerInfo from './BookCustomerInfo';
import BookChatTime from './BookChatTime';
import BookPayment from './BookPayment';
import BookComplete from './BookComplete';
import { IBookInfo } from '../../../types/IMeeting';
import styles from './stepper.module.scss';

const BookingStepper = () => {
  const isDesktop = useMediaQuery('(min-width: 900px)');

  function getSteps() {
    return [
      'Customer Information',
      'Choose Chatting Time',
      'Payment',
      'Complete',
    ];
  }

  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  const expertID = useQuery().get('expertId') || '';
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchExpertById(expertID));
  }, []);
  const userEmail = useSelector(selectCurrentUserEmail);
  const userFirstName = useSelector(selectCurrentUserFirstName);
  const userLastName = useSelector(selectCurrentUserLastName);
  const expert = useSelector(selectCurrentExpertDetails);
  const userID = useSelector(selectCurrentUserId);

  const BookInfoInitial = {
    expertID,
    userID,
    firstName: userFirstName,
    lastName: userLastName,
    email: userEmail,
    nameOnCard: '',
    cardNumber: null,
    CVVCode: null,
    expiration: null,
    date: new Date(),
    time: null,
    price: expert.price,
  };

  const [activeStep, setActiveStep] = React.useState(0);
  const step = getSteps();
  const [bookingInfo, setBookingInfo] = useState<IBookInfo>(BookInfoInitial);

  const handleChange = (
    input: string,
  ) => (
    e: { target: { value: string; }; },
  ) => (setBookingInfo({ ...bookingInfo, [input]: e.target.value }));

  const handleDate = (input: string) => (newValue) => (
    setBookingInfo({ ...bookingInfo, [input]: newValue })
  );

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleTimeSelect = (input: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setBookingInfo({ ...bookingInfo, [input]: (event.target as HTMLInputElement).value });
  };

  const handleSubmit = () => {
    dispatch(createMeeting(bookingInfo));
    setActiveStep(activeStep + 1);
  };

  function getStepContent(steps: Number) {
    switch (steps) {
      case 0:
        return (
          <BookCustomerInfo
            handleNext={handleNext}
            handleChange={handleChange}
            bookDetail={bookingInfo}
          />
        );
      case 1:
        return (
          <BookChatTime
            handleNext={handleNext}
            handlePrevious={handleBack}
            handleChange={handleDate}
            handleTimeSelect={handleTimeSelect}
            bookDetail={bookingInfo}
          />
        );
      case 2:
        return (
          <BookPayment
            handleSubmit={handleSubmit}
            handlePrevious={handleBack}
            handleChange={handleChange}
            bookDetail={bookingInfo}
          />
        );
      default:
        return null;
    }
  }

  return (
    <Box className={styles.stepperWrapper}>
      {isDesktop && (
        <Stepper activeStep={activeStep}>
          {step.map((label) => {
            const stepProps: { completed?: boolean } = {};
            const labelProps: {
              optional?: React.ReactNode;
            } = {};

            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
      )}
      <div className={styles.stepperMain}>
        <div className={styles.formWrapper}>
          {activeStep === step.length - 1 ? (
            <BookComplete />
          ) : (getStepContent(activeStep))}
        </div>
        {isDesktop && <ExpertCard expertInfo={expert} isButtonVisible={false} />}
      </div>
    </Box>
  );
};

export default BookingStepper;
