import React from 'react';
import Header from 'layouts/Header';
import Footer from 'layouts/Footer';
import useMediaQuery from '@mui/material/useMediaQuery';
import { Container } from '@mui/material';
import { Breadcrumb } from 'components';
import { useSelector } from 'react-redux';
import { selectCurrentUserRole } from 'store/slices/authSlice';
import { selectCurrentUserEmail, selectCurrentUserFirstName } from 'store/slices/userSlice';
import { useHistory } from 'react-router-dom';
import ProfileTabs from './ProfileTabs';
import styles from './ProfilePage.module.scss';

const ProfilePage = () => {
  const history = useHistory();
  // get login user basic information: role, email and firstname
  const isAuthenticated = useSelector(selectCurrentUserRole);
  const email = useSelector(selectCurrentUserEmail) as string;
  const firstName = useSelector(selectCurrentUserFirstName) as string;
  // redirect to home page if cannot read token
  if (!isAuthenticated || email == null || firstName == null) {
    history.push('/');
  }

  const matchMobile = useMediaQuery('(min-width:900px)');

  return (
    <>
      <Header />
      <Container className={styles.profile} maxWidth="lg">
        {matchMobile && <Breadcrumb />}
        <ProfileTabs email={email} firstName={firstName} />
      </Container>
      <Footer />
    </>
  );
};

export default ProfilePage;
