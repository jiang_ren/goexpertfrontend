import React, { useState } from 'react';
import { Alert, Button } from '@mui/material';
import { styled } from '@mui/material/styles';
import { getImageUploadUrl, putImage, saveImageKey } from 'services/profile';
import { get as storageGet } from 'utils/localStorage';
import { TOKEN } from 'constants/localStorageKeys';
import { useSelector } from 'react-redux';
import { selectCurrentUserID } from 'store/slices/userSlice';

const Input = styled('input')({
  display: 'none',
});

const ImageUpload = () => {
  const [imageData, setImageData] = useState<File>();
  const [alertMsg, setAlertMsg] = useState('');

  const userID = useSelector(selectCurrentUserID) as string;
  const token = JSON.parse(storageGet(TOKEN));

  const onImageInputChange = (event) => {
    setImageData(event.target.files[0]);
  };

  const handleImageSubmit = async () => {
    if (!imageData) {
      setAlertMsg('No image seleted');
    }
    const { uploadURL, Key } = await getImageUploadUrl();
    putImage(uploadURL, imageData as File)
      .then(() => {
        saveImageKey(Key, token, userID);
        setAlertMsg('');
      })
      .catch(() => {
        setAlertMsg('Server Error');
      });
  };

  return (
    <div>
      <label htmlFor="imageInput">
        <Input
          accept="image/*"
          id="imageInput"
          type="file"
          onChange={onImageInputChange}
        />
        <Button variant="contained" component="span">
          Upload
        </Button>
      </label>
      {imageData && <p>{imageData.name}</p>}
      <Button variant="contained" component="span" onClick={handleImageSubmit}>
        Submit
      </Button>

      {alertMsg && <Alert severity="warning">{alertMsg}</Alert>}
    </div>
  );
};

export default ImageUpload;
