import React from 'react';
import ImageUpload from '../ImageUpload';

const AccountSettingsPage = () => {
  return (
    <div>
      <h2>Account Settings</h2>
      <h3>Set Your Avatar</h3>
      <ImageUpload />
    </div>
  );
};

export default AccountSettingsPage;
